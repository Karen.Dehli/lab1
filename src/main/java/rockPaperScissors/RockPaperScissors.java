package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
        System.out.println("Bye bye :)");
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String result;
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        while (true){
            System.out.println("Let's play round "+roundCounter);
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            //har en liste med rock paper scissors, hvordan bruke denne?
            while (!rpsChoices.contains(humanChoice)){
                System.out.println("I don't understand "+humanChoice+". Could you try again?");
                humanChoice=readInput("Your choice (Rock/Paper/Scissors)");
                System.out.println(humanChoice);
            }
            String computerChoice=computer();
            if ((humanChoice=="rock"&&computerChoice=="scissors")||(humanChoice=="paper"&&computerChoice=="rock")||(humanChoice=="scissors")&&(computerChoice=="paper")){
                result="Human wins!";
                humanScore++;
            }
            else if (humanChoice==computerChoice){
                result="It's a tie!";
            }
            else{
                result="Computer wins!";
                computerScore++;
            }

            System.out.println("Human chose "+humanChoice+", computer chose "+computerChoice+". "+result);
            System.out.println("Score: human "+humanScore+", computer "+computerScore);

            String Continue=continueCheck();
            //== er ikke det samme som i python!! bruk .equals
            if ("n".equals(Continue)){
                break;
            }
            
            roundCounter++;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }


    public String computer(){
        Random r = new Random();
        int index= r.nextInt(3);
        
        return rpsChoices.get(index);
    }

    public String continueCheck(){
        String check=readInput("Do you wish to continue playing? (y/n)?");
        return check;
    }

}
